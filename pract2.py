import datetime

from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.exc import NoResultFound
from marshmallow import Schema, fields, ValidationError, pre_load, post_load, pre_dump, post_dump

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:////quotes.db"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

##### MODELS #####


class Author(db.Model):  # type: ignore
    id = db.Column(db.Integer, primary_key=True)
    first = db.Column(db.String(80))
    last = db.Column(db.String(80))


class Quote(db.Model):  # type: ignore
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String, nullable=False)
    author_id = db.Column(db.Integer, db.ForeignKey("author.id"))
    author = db.relationship("Author", backref=db.backref("quotes", lazy="dynamic"))
    posted_at = db.Column(db.DateTime)


##### SCHEMAS #####


class AuthorSchema(Schema):
    id = fields.Int(dump_only=True)
    first = fields.Str()
    last = fields.Str()
    formatted_name = fields.Method("format_name", dump_only=True)

    def format_name(self, author):
        return f"{author.last}, {author.first}"


# Custom validator
def must_not_be_blank(data):

    print("In Validation ...")
    if not data:
        raise ValidationError("Data not provided.")


class QuoteSchema(Schema):
    id = fields.Int(dump_only=True)
    #author = fields.Nested(AuthorSchema, validate=must_not_be_blank)
    author = fields.Dict()
    content = fields.Str(required=True, validate=must_not_be_blank)
    posted_at = fields.DateTime()

    # Allow client to pass author's full name in request body
    # e.g. {"author': 'Tim Peters"} rather than {"first": "Tim", "last": "Peters"}
    @pre_load
    def process_author(self, data, **kwargs):
        author_name = data.get("author")
        if author_name:
            first, last = author_name.split(" ")
            author_dict = dict(first=first, last=last)
        else:
            author_dict = {}
        data["author"] = author_dict

        #print("Data in preload : ",data)

        return data

    @post_load
    def post_load_process(self, data, **kwargs):
        #print("In Post_load : ", data)
        return data

    @pre_dump # Input data will be recieved as input, we can do any prcessing here, re formatting or changes.
    def pre_dump_process(self, data, **kwargs):

        print("Data in pre dump : ",data)
        return data

    @post_dump # Here Data will be recieved as converted from python level object or structure to simple dict
    def post_dump_process(self, data, **kwargs):

        print("Data in post dump", data)
        return data


author_schema = AuthorSchema()
authors_schema = AuthorSchema(many=True)
quote_schema = QuoteSchema()
quotes_schema = QuoteSchema(many=True, only=("id", "content"))


print("Adding some changes to demonstration ....")
print("Making some changes in local repository ....")
###############################################################

demo_data = {"author":"Sulal Akbar","content":"No Content","posted_at":"2022-08-02T05:32:20.772296"}

print("Calling Load ...")
data = quote_schema.load(demo_data)

print("Calling Dump ...")
try:

    dump_data = quote_schema.dump(data)
except Exception as e:
    print("Error in Dump Call : ",e)
print("Dump Data : ",dump_data)


